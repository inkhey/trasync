from functools import partial
from typing import Callable

import aiohttp
from aiohttp_sse_client import client as sse_client
import uritemplate


def _url(endpoint: str, base_url: str) -> str:
    return base_url + endpoint


class Trasync:
    def __init__(self, session: aiohttp.ClientSession, base_url: str):
        self.session = session
        self._get_url = partial(_url, base_url=base_url)
        self.contents = ContentsAction(session, self._get_url)
        self.auth = AuthAction(session, self._get_url)
        self.users = UsersAction(session, self._get_url)


class Action:
    def __init__(self, session: aiohttp.ClientSession, url_getter: Callable):
        self._session = session
        self._get_url = url_getter
        self._post_init()

    def _post_init(self):
        pass


class AuthAction(Action):
    WHOAMI_ENDPOINT = "/api/auth/whoami"

    def whoami(self):
        return self._session.get(self._get_url(self.WHOAMI_ENDPOINT))


class ContentsAction(Action):
    ALL_ENDPOINT = "/api/workspaces{/workspace_id}/contents"
    ONE_ENDPOINT = "/api/workspaces{/workspace_id}/contents{/content_id}"

    async def get_one(self, workspace_id: int, content_id: int) -> aiohttp.ClientResponse:
        url = self._get_url(self.ONE_ENDPOINT)
        url = uritemplate.URITemplate(url).expand(
            workspace_id=str(workspace_id), content_id=str(content_id)
        )
        return await self._session.get(url)

    async def get_all(self, workspace_id: int) -> aiohttp.ClientResponse:
        url = self._get_url(self.ALL_ENDPOINT)
        url = uritemplate.URITemplate(url).expand(workspace_id=str(workspace_id))
        return await self._session.get(url)

    def _post_init(self):
        self.comments = CommentsAction(self._session, self._get_url)


class CommentsAction(Action):
    ALL_ENDPOINT = "/api/workspaces{/workspace_id}/contents{/content_id}/comments"

    async def get_all(self, workspace_id: int, content_id: int) -> aiohttp.ClientResponse:
        url = self._get_url(self.ALL_ENDPOINT)
        url = uritemplate.URITemplate(url).expand(
            workspace_id=str(workspace_id), content_id=str(content_id)
        )
        return await self._session.get(url)

    async def create(
        self, workspace_id: int, content_id: int, raw_content: str
    ) -> aiohttp.ClientResponse:
        url = self._get_url(self.ALL_ENDPOINT)
        url = uritemplate.URITemplate(url).expand(
            workspace_id=str(workspace_id), content_id=str(content_id)
        )
        return await self._session.post(url, json={"raw_content": raw_content})


class UsersAction(Action):
    ALL_ENDPOINT = "/api/users"
    ONE_ENDPOINT = "/api/users{/user_id}"
    LIVE_MESSAGES = "/api/users{/user_id}/live_messages"
    MESSAGES = "/api/users{/user_id}/messages"

    async def get_all(self) -> aiohttp.ClientResponse:
        url = self._get_url(self.ALL_ENDPOINT)
        return await self._session.get(url)

    async def get_one(self, user_id: int) -> aiohttp.ClientResponse:
        url = self._get_url(self.ONE_ENDPOINT)
        url = uritemplate.URITemplate(url).expand(user_id=str(user_id))
        return await self._session.get(url)

    async def get_messages(self, user_id: int) -> aiohttp.ClientResponse:
        url = self._get_url(self.MESSAGES)
        url = uritemplate.URITemplate(url).expand(user_id=str(user_id))
        return await self._session.get(url)

    def get_live_messages(self, user_id: int) -> sse_client.EventSource:
        url = self._get_url(self.LIVE_MESSAGES)
        url = uritemplate.URITemplate(url).expand(user_id=str(user_id))
        return sse_client.EventSource(url, session=self._session)
