import asyncio

import aiohttp
from aioresponses import aioresponses

from trasync import Trasync


def test_whoami():
    loop = asyncio.get_event_loop()
    session = aiohttp.ClientSession()
    trasync = Trasync(session, base_url="http://localhost:8080")
    with aioresponses() as mocked:
        mocked.get("http://localhost:8080/api/auth/whoami", status=200, body="{}")
        resp = loop.run_until_complete(trasync.auth.whoami())
        assert resp.status == 200
