import asyncio
import json
import pprint
import aiohttp

from trasync.tracim_session import Trasync


async def main():
    auth=aiohttp.BasicAuth(
        login="admin@admin.admin",
        password="admin@admin.admin"
    )
    client_session = aiohttp.ClientSession(auth=auth)
    trasync = Trasync(session=client_session, base_url='http://localhost:8080')

    result = await trasync.contents.get_all(workspace_id=1)
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.contents.get_one(workspace_id=1,content_id=1)
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.contents.comments.get_all(workspace_id=1,content_id=1)
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.contents.comments.create(workspace_id=1, content_id=1, raw_content='super !')
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.auth.whoami()
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.users.get_all()
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.users.get_one(user_id=1)
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    result = await trasync.users.get_messages(user_id=1)
    print(result)
    json_response = await result.json()
    pprint.pprint(json_response)

    async with trasync.users.get_live_messages(user_id=1) as event_source:
        try:
            await asyncio.sleep(0.01)
            async for event in event_source:
                print(event.type)
                if event.type == 'message':
                    data = json.loads(event.data)
                    pprint.pprint(data)
                    break
        except ConnectionError:
            pass

    await client_session.close()


asyncio.get_event_loop().run_until_complete(main())
